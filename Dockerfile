FROM alpine:3.10

RUN apk add --no-cache mariadb=10.3.17-r0 mariadb-client

RUN apk add --no-cache shadow gettext && groupadd -r mariadb && useradd -r -g mariadb mariadb

RUN chown -R mariadb:mariadb /var/lib/mysql && mkdir -p /run/mysqld && chown -R mariadb:mariadb /run/mysqld

RUN mkdir /docker-entrypoint-initdb.d

VOLUME ["/var/lib/mysql", "/docker-entrypoint-initdb.d"]

ADD entrypoint.sh /usr/local/bin/entrypoint.sh

ADD etc/my.cnf.d/mariadb-server.cnf /etc/my.cnf.d/mariadb-server.cnf

RUN chmod 0755 /usr/local/bin/entrypoint.sh

EXPOSE 3306

ENTRYPOINT ["entrypoint.sh"]

CMD ["mysqld"]
