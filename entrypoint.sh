#!/bin/sh

set -e
if [ "${1:0:1}" = '-' ]; then
  set -- mysqld "$@"
fi

if [ "$1" = 'mysqld' ]; then
  # read DATADIR from the MariaDB config
  DATADIR="$("$@" --verbose --help 2>/dev/null | awk '$1 == "datadir" { print $2; exit }')"
					
  if [ ! -d "$DATADIR/mariadb" ]; then
    if [ -z "$MARIADB_ROOT_PASSWORD" -a -z "$MARIADB_ALLOW_EMPTY_PASSWORD" ]; then
	  echo >&2 'error: database is uninitialized and MARIADB_ROOT_PASSWORD not set'
	  echo >&2 '  Did you forget to add -e MARIADB_ROOT_PASSWORD=... ?'
	  exit 1
    fi
																						
    echo 'Initializing database'
    mysql_install_db --datadir="$DATADIR"
    echo 'Database initialized'
																										
    # These statements _must_ be on individual lines, and _must_ end with
    # semicolons (no line breaks or comments are permitted).
    # TODO proper SQL escaping on ALL the things D:																				
    tempSqlFile='/tmp/mariadb-first-time.sql'
    cat > "$tempSqlFile" <<-EOSQL
DELETE FROM mysql.user ;
CREATE USER 'root'@'%' IDENTIFIED BY '${MARIADB_ROOT_PASSWORD}' ;
GRANT ALL ON *.* TO 'root'@'%' WITH GRANT OPTION ;
DROP DATABASE IF EXISTS test ;

EOSQL
   
    cat $tempSqlFile

    if [ "$MARIADB_DATABASE" ]; then
	  echo "CREATE DATABASE IF NOT EXISTS \`$MARIADB_DATABASE\` ;" >> "$tempSqlFile"
    fi

    if [ "$MARIADB_USER" -a "$MARIADB_PASSWORD" ]; then
      echo "CREATE USER '$MARIADB_USER'@'%' IDENTIFIED BY '$MARIADB_PASSWORD' ;" >> "$tempSqlFile"
      if [ "$MARIADB_DATABASE" ]; then
        echo "GRANT ALL ON \`$MARIADB_DATABASE\`.* TO '$MARIADB_USER'@'%' ;" >> "$tempSqlFile"
      fi
    fi

    echo 'FLUSH PRIVILEGES ;' >> "$tempSqlFile"

    for file in /docker-entrypoint-initdb.d/*; do
      case "$file" in
        *.sql)    echo "$0: copy $file into init-file"; cat $file >> $tempSqlFile ;;
        *.sql.gz) echo "$0: copy $file into init-file"; gunzip -c $file >> $tempSqlFile ;;
        *)        echo "$0: ignoring $file" ;;
      esac

      echo
    done

    set -- "$@" --init-file="$tempSqlFile"
  fi

  #ensure that directory is created and database is only initialized once
  mkdir -p "$DATADIR"/mariadb

  chown -R mariadb:mariadb "$DATADIR"

fi

set -- "$@" --user=mariadb
set -- "$@" --verbose

exec "$@"
